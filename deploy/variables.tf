variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "homer@jbplab.com"
}

variable "db_username" {
  description = "username for the PostgreSQL database instance"
}

variable "db_password" {
  description = "password for the PostgreSQL database instance"
}

variable "bastion_key_name" {
  default = "recipe-app-api-devops-bastion"
}